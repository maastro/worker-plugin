package nl.maastro.mia.workerplugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.AfterClass;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.mathworks.toolbox.javabuilder.Disposable;
import com.netflix.discovery.EurekaClient;

import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;

/** 
 * Abstract base class for unit testing of computation modules 
 */
public abstract class AbstractModuleTest {
    
    private static Path tempPath = Paths.get(".\\temp\\test");
    
    protected static Disposable matlabInstance;
    protected static Set<Disposable> disposableMWObjects = new HashSet<>();
    
    @MockBean
    EurekaClient eurekaClient;
    
    @AfterClass
    public static void teardown() throws IOException {
        if (matlabInstance != null) {
            matlabInstance.dispose();
        }
        disposableMWObjects.stream().forEach(disposable -> disposable.dispose());
        deleteTempFolder(tempPath);
    }
    
    protected ComputationResult performComputation(
            ComputationInterface module,
            String configuration,
            Map<String, Object> data, 
            Map<String, String> inputModalities,
            List<String> outputModalities) throws Exception {
        ComputationData computationData = getComputationData(
                configuration, data, inputModalities, outputModalities);
        ComputationResult result = module.performComputation(computationData);
        disposableMWObjects.addAll(result.getDisposables());
        return result;
    }
    
    private ComputationData getComputationData(
            String configuration,
            Map<String, Object> data,
            Map<String, String> inputModalities,
            List<String> outputModalities) throws Exception {
        
        ComputationData computationData = new ComputationData();
        computationData.setMatlabInstance(matlabInstance);
        computationData.setConfiguration(configuration);
        computationData.getData().putAll(data);
        computationData.setInputModalities(inputModalities);
        computationData.setOutputModalities(outputModalities);
        computationData.setTempPath(tempPath.toString());
        return computationData;
    }
    
    private static void deleteTempFolder(Path tempFolderPath) throws IOException {
        File tempFolder = tempFolderPath.toFile();
        if (tempFolder.exists()) {
            FileUtils.deleteDirectory(tempFolderPath.toFile());
        }
    }

}
