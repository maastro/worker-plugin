package nl.maastro.mia.workerplugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.test.context.junit4.SpringRunner;

import nl.maastro.mia.workerplugin.service.ExportService;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@EnableEurekaClient
public class FileExportTests {
	
	private static final List<String> TEST_FILES = Arrays.asList(
			"\\\\as-bld-01\\testdata\\12345\\RtDose\\FO-3153671375338877408_v2.dcm", 
			"\\\\as-bld-01\\testdata\\12345\\RtPlan\\FO-3630512758406762316.dcm",
			"\\\\as-bld-01\\testdata\\12345\\RtStruct\\FO-4073997332899944647.dcm");
	private static final int PORT = 9001;
	
	@Autowired
	private ExportService exportService;
	
	// Meant as an integration test; needs a file service running with 
	// storescp started on port 9001
	@Ignore
	@Test
	public void testExportFilesToFileService() throws Exception {
		List<File> dicomFiles = new ArrayList<>();
		TEST_FILES.forEach(f -> dicomFiles.add(new File(f)));
		exportService.exportFilesToFileService(dicomFiles, PORT);
	}
}
