package nl.maastro.mia.workerplugin;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mathworks.toolbox.javabuilder.Disposable;
import com.mathworks.toolbox.javabuilder.MWCellArray;
import com.mathworks.toolbox.javabuilder.MWStructArray;

import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.service.MatlabService;

public class MatlabDisposeTest {
    
    private static final String VALUE_NOT_DISPOSED = "this struct is not disposed";
    
    private MatlabService matlabService = new MatlabService();
    private List<Disposable> disposables = new ArrayList<>();
    private MWCellArray cellArray;
    private MWStructArray mwStructArray;

    @Test(expected = IllegalStateException.class)
    public void cellArrayDisposed(){
        cellArray.get(1);
    }

    @Test(expected = IllegalStateException.class)
    public void structDisposed(){
        cellArray.get(2);
    }

    @Test
    public void embeddedObjectsAreNotDisposed() {
        char[][] struct = (char[][]) mwStructArray.get(1);
        String value = new String(struct[0]);
        assertEquals(VALUE_NOT_DISPOSED, value);
    }

    @Test
    public void disposeDataContainer() {
        DataContainer dataContainer = new DataContainer();
        dataContainer.setMatlabInstance(() -> {});
        dataContainer.getDisposables().add(() -> {});
        dataContainer.getDisposables().add(null);

        matlabService.dispose(dataContainer);
    }

    @Before
    public void initAndDispose() {
        cellArray = new MWCellArray(2, 1);
        cellArray.set(1, "hello");

        mwStructArray = new MWStructArray(1, 1, new String[]{"c"});
        mwStructArray.set(1, VALUE_NOT_DISPOSED);
        cellArray.set(2, mwStructArray);

        disposables.add(cellArray);
        disposables.forEach(disposable -> disposable.dispose());
    }

}
