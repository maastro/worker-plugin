package nl.maastro.mia.workerplugin.container;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mathworks.toolbox.javabuilder.Disposable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComputationData {
	
	private String identifier;
	private long containerId;
	private String patientId;
	private String planLabel;
	private String tempPath;

	@JsonIgnore
	private Disposable matlabInstance;

	@JsonIgnore
	private Map<String, Object> data = new HashMap<>();
	private String configuration;
	private List<VolumeOfInterest> volumesOfInterest = new ArrayList<>();

	@JsonIgnore
	private Map<String, String> inputModalities;

	@JsonIgnore
	private List<String> outputModalities;

	private Map<String, List<Image>> images = new HashMap<>();

	public ComputationData() {}
	
	public ComputationData(DataContainer dataContainer,	Computation computation) {
		
		this.identifier = computation.getIdentifier();
		this.containerId = dataContainer.getContainerId();
		this.patientId = dataContainer.getPatientId();
		this.planLabel = dataContainer.getPlanLabel();
		this.tempPath = dataContainer.getTempPath();
		this.matlabInstance = dataContainer.getMatlabInstance();
		this.data = dataContainer.getData();
		this.images = dataContainer.getImages();
		this.configuration = computation.getConfiguration();
		
		for (String voiName : computation.getVolumeOfInterestNames()) {
			this.volumesOfInterest.add(dataContainer.getVolumesOfInterest().get(voiName));
		}
		
        this.inputModalities = computation.getInputModalities();
        this.outputModalities = computation.getOutputModalities();
	}
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public long getContainerId() {
		return containerId;
	}
	
	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getPlanLabel() {
		return planLabel;
	}

	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}
	
	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public Disposable getMatlabInstance() {
		return matlabInstance;
	}

	public void setMatlabInstance(Disposable matlabInstance) {
		this.matlabInstance = matlabInstance;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public List<VolumeOfInterest> getVolumesOfInterest() {
		return volumesOfInterest;
	}

	public void setVolumesOfInterest(List<VolumeOfInterest> volumesOfInterest) {
		this.volumesOfInterest = volumesOfInterest;
	}

	public Map<String, List<Image>> getImages() {
		return images;
	}

	public void setImages(Map<String, List<Image>> images) {
		this.images = images;
	}
	
	@Override
	public String toString() {
		return "ComputationData [identifier=" + identifier + ", containerId=" + containerId + ", patientId=" + patientId
				+ ", planLabel=" + planLabel + ", tempPath=" + tempPath + ", matlabInstance=" + matlabInstance + ", data=" + data
				+ ", configuration=" + configuration + ", volumesOfInterest=" + volumesOfInterest + ", images=" + images
				+ "]";
	}

	public Map<String, Object> getData() {
		return data;
	}

	public Map<String, String> getInputModalities() {
		return inputModalities;
	}

	public void setInputModalities(Map<String, String> inputModalities) {
		this.inputModalities = inputModalities;
	}

	public List<String> getOutputModalities() {
		return outputModalities;
	}

	public void setOutputModalities(List<String> outputModalities) {
		this.outputModalities = outputModalities;
	}
}