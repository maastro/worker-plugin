package nl.maastro.mia.workerplugin.container;

import java.util.ArrayList;
import java.util.List;

public class VolumeOfInterest {
	
	String name;
	List<String> rois = new ArrayList<>();
	List<String> operators = new ArrayList<>();
	
	public List<String> getOperators() {
		return operators;
	}
	
	public void setOperators(List<String> operators) {
		this.operators = operators;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<String> getRois() {
		return rois;
	}

	public void setRois(List<String> rois) {
		this.rois = rois;
	}

	@Override
	public String toString() {
		return "VolumeOfInterest [name=" + name 
				+ ", rois=" + rois 
				+ ", operators=" + operators + "]";
	}

	
}