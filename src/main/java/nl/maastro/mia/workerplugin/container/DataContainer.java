package nl.maastro.mia.workerplugin.container;

import java.util.*;

import com.mathworks.toolbox.javabuilder.Disposable;

public class DataContainer {
	private String taskId = UUID.randomUUID().toString();
	private Long containerId;
	private String patientId;
	private String planLabel;
	private Date creationTime = new Date();
	private String tempPath;
	
	private Disposable matlabInstance;
	private final Map<String,Object> data = new HashMap<>();
	private final Set<Disposable> disposables = new HashSet<>();

	private Map<String, List<Image>> images = new HashMap<>();
	
	private List<String> roiNames = new ArrayList<>();
	private Map<String,String> mappingRtogRois = new HashMap<>();
	private Map<String, VolumeOfInterest> volumesOfInterest = new HashMap<>();
	
	private String initializingServiceName;
	private Integer outputPort;
	private List<ModuleConfiguration> moduleConfigurations = new ArrayList<>();
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getPlanLabel() {
		return planLabel;
	}

	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}
	
	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	
	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}
	
	public Disposable getMatlabInstance() {
		return matlabInstance;
	}

	public void setMatlabInstance(Disposable matlabInstance) {
		this.matlabInstance = matlabInstance;
	}
	
	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data.clear();
		this.data.putAll(data);
	}
	
	public Map<String, List<Image>> getImages() {
		return images;
	}

	public void setImages(Map<String, List<Image>> images) {
		this.images.clear();
		this.images.putAll(images);
	}
	
	public List<String> getRoiNames() {
		return roiNames;
	}
	
	public void setRoiNames(List<String> roiNames) {
		this.roiNames = roiNames;
	}

	public void setRoiNames(Collection<String> roiNames) {
		this.roiNames.clear();
		this.roiNames.addAll(roiNames);
	}
	
	public Map<String, String> getMappingRtogRois() {
		return mappingRtogRois;
	}

	public void setMappingRtogRois(Map<String, String> mappingRtogRois) {
		this.mappingRtogRois.clear();
		this.mappingRtogRois.putAll(mappingRtogRois);
	}
	
	public Map<String, VolumeOfInterest> getVolumesOfInterest() {
		return volumesOfInterest;
	}

	public void setVolumesOfInterest(Map<String, VolumeOfInterest> volumesOfInterest) {
		this.volumesOfInterest = volumesOfInterest;
	}
	
	public String getInitializingServiceName() {
		return initializingServiceName;
	}

	public void setInitializingServiceName(String initializingService) {
		this.initializingServiceName = initializingService;
	}

	public Integer getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(Integer outputPort) {
		this.outputPort = outputPort;
	}

	public List<ModuleConfiguration> getModuleConfigurations() {
		return moduleConfigurations;
	}

	public void setModuleConfigurations(List<ModuleConfiguration> moduleConfigurations) {
		this.moduleConfigurations = moduleConfigurations;
	}

	public Set<Disposable> getDisposables() {
		return disposables;
	}

	@Override
	public String toString() {
		return "DataContainer [taskId=" + taskId + ", containerId=" + containerId + ", patientId="
				+ patientId + ", creationTime=" + creationTime + ", images="
				+ images + ", roiNames=" + roiNames + ", mappingRtogRois=" + mappingRtogRois + ", volumesOfInterest="
				+ volumesOfInterest + ", initializingServiceName=" + initializingServiceName + ", configuration="
				+ moduleConfigurations + "]";
	}
}