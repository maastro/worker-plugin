package nl.maastro.mia.workerplugin.container;

import java.util.ArrayList;
import java.util.List;

public class ModuleConfiguration {
	
	private String moduleName;
	private List<Computation> computations = new ArrayList<>();
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String identifier) {
		this.moduleName = identifier;
	}
	public List<Computation> getComputations() {
		return computations;
	}
	public void setComputations(List<Computation> calculations) {
		this.computations = calculations;
	}
	@Override
	public String toString() {
		return "ModuleConfiguration [moduleName=" + moduleName + ", computations=" + computations + "]";
	}
	
}