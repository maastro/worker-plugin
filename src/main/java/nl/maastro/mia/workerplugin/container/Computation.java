package nl.maastro.mia.workerplugin.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Computation {
	
	private String identifier;
	private String configuration;
	private List<String> volumeOfInterestNames = new ArrayList<>();
	private List<String> outputModalities = new ArrayList<>();
	private Map<String, String> inputModalities = new HashMap<>();

	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getConfiguration() {
		return configuration;
	}
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	public List<String> getVolumeOfInterestNames() {
		return volumeOfInterestNames;
	}
	public void setVolumeOfInterestNames(List<String> volumeOfInterestNames) {
		this.volumeOfInterestNames = volumeOfInterestNames;
	}
	@Override
	public String toString() {
		return "Computation [identifier=" + identifier + ", configuration=" + configuration + ", volumeOfInterestNames="
				+ volumeOfInterestNames + "]";
	}
	public List<String> getOutputModalities() {
		return outputModalities;
	}
	public void setOutputModalities(List<String> outputModalities) {
		this.outputModalities = outputModalities;
	}
	public Map<String, String> getInputModalities() {
		return inputModalities;
	}
	public void setInputModalities(Map<String, String> inputModalities) {
		this.inputModalities = inputModalities;
	}
	
}