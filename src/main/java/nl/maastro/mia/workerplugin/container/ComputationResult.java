package nl.maastro.mia.workerplugin.container;

import com.mathworks.toolbox.javabuilder.Disposable;

import java.util.*;

public class ComputationResult {
	
    String computationIdentifier;
	List<VolumeOfInterest> volumesOfInterest;
	String error = null;
	final Map<String,String> values = new HashMap<>();
	final Map<String,Object> generatedObjects = new HashMap<>();
	private List<String> generatedDicomFiles = new ArrayList<>();
	Set<Disposable> disposables = new HashSet();
	String version;
	String moduleName;

	private boolean exportResult = false;

	private boolean exportDicom = false;
	
	public void addComputationData(ComputationData computationData,
			String serviceName,
			Computation computation) {
		this.volumesOfInterest = computationData.getVolumesOfInterest();
		this.moduleName = serviceName;
		this.computationIdentifier = computation.getIdentifier();
	}

	public String getComputationIdentifier() {
        return computationIdentifier;
    }

    public void setComputationIdentifier(String computationIdentifier) {
        this.computationIdentifier = computationIdentifier;
    }

    public List<VolumeOfInterest> getVolumesOfInterest() {
		return volumesOfInterest;
	}

	public void setVolumesOfInterest(List<VolumeOfInterest> volumesOfInterest) {
		this.volumesOfInterest = volumesOfInterest;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values.clear();
		this.values.putAll(values);
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void addValue(String key, String result){
		this.values.put(key, result);
	}

	public Map<String, Object> getGeneratedObjects() {
		return generatedObjects;
	}

	public void setGeneratedObjects(Map<String,Object> dicomObjects){
		this.generatedObjects.clear();
		this.generatedObjects.putAll(dicomObjects);
	}

	public void addGeneratedObject(String key, Object value){
		this.generatedObjects.put(key, value);
	}

	public void addGeneratedDicomFile(String filePath){
		this.generatedDicomFiles.add(filePath);
	}

	public List<String> getGeneratedDicomFiles(){
		return this.generatedDicomFiles;
	}

	public void clearGeneratedDicomFiles(){
		this.generatedDicomFiles.clear();
	}

	public boolean isExportResult() {
        return exportResult;
    }

    public void setExportResult(boolean exportResult) {
        this.exportResult = exportResult;
    }

	public Set<Disposable> getDisposables() {
		return disposables;
	}

	public void setDisposables(Set<Disposable> disposables) {
		this.disposables = disposables;
	}

	public boolean isExportDicom() {
		return exportDicom;
	}

	public void setExportDicom(boolean exportDicom) {
		this.exportDicom = exportDicom;
	}

	@Override
    public String toString() {
        return "ComputationResult [computationIdentifier=" + computationIdentifier + ", volumesOfInterest="
                + volumesOfInterest + ", error=" + error + ", values=" + values + ", version=" + version
                + ", moduleName=" + moduleName + ", exportResult=" + exportResult + "]";
    }
}