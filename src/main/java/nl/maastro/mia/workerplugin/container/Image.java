package nl.maastro.mia.workerplugin.container;

public class Image {
	
	private String location;
	private String sopUid;
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getSopUid() {
		return sopUid;
	}
	
	public void setSopUid(String sopUid) {
		this.sopUid = sopUid;
	}

	@Override
	public String toString() {
		return "Image [location=" + location + ", sopUid=" + sopUid + "]";
	}
}
