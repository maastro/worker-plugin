
package nl.maastro.mia.workerplugin.constants;

public class DicomConcepts {
	public static final String RTSTRUCT = "RTSTRUCT";
	public static final String RTDOSE = "RTDOSE";
	public static final String RTPLAN = "RTPLAN";
	public static final String CT = "CT";
	
	private DicomConcepts(){
	}
}