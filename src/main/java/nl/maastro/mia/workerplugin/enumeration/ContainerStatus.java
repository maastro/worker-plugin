package nl.maastro.mia.workerplugin.enumeration;

public enum ContainerStatus {
    QUEUE,RUNNING,DONE,CALCULATIONERROR,EXPORTERROR
}
