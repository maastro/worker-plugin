package nl.maastro.mia.workerplugin.web.dto.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DicomPackageDto {
	String patientId;
	String planlabel;
	List<String> roiNames;
	
	Map<String, List<ImageDto>> images;

	public DicomPackageDto() {
		roiNames = new ArrayList<>();
		images = new HashMap<>();
	}

	public DicomPackageDto(String patientId,
			String planLabel,
			List<String> roiNames,
			Map<String, List<ImageDto>> images) {
		super();
		this.patientId = patientId;
		this.planlabel = planLabel;
		this.roiNames = roiNames;
		this.images = images;
	}


	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public List<String> getRoiNames() {
		return roiNames;
	}

	public void setRoiNames(List<String> roiNames) {
		this.roiNames = roiNames;
	}

	public Map<String, List<ImageDto>> getImages() {
		return images;
	}

	public void setImages(Map<String, List<ImageDto>> images) {
		this.images = images;
	}
	
	public String getPlanlabel() {
		return planlabel;
	}

	public void setPlanlabel(String planlabel) {
		this.planlabel = planlabel;
	}
}