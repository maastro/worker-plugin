package nl.maastro.mia.workerplugin.web.dto.container;

import java.util.List;

public class ModuleConfigurationDto {
	private String moduleName;
	private List<ComputationDto> computations;
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String identifier) {
		this.moduleName = identifier;
	}
	public List<ComputationDto> getComputations() {
		return computations;
	}
	public void setComputations(List<ComputationDto> calculations) {
		this.computations = calculations;
	}
}