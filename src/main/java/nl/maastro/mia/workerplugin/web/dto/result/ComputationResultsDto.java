package nl.maastro.mia.workerplugin.web.dto.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComputationResultsDto {
	
	String taskId;
	String containerId;
	String patientId;
	String planLabel;
	Map<String, List<String>> sopUids;
	Map<String, String> mappingRtogRoi = new HashMap<>();
	
	List<ComputationResultDto> computationResults = new ArrayList<>();
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getPlanLabel() {
		return planLabel;
	}

	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}
	
	public Map<String, List<String>> getSopUids() {
		return sopUids;
	}

	public void setSopUids(Map<String, List<String>> sopUids) {
		this.sopUids = sopUids;
	}

	public Map<String, String> getMappingRtogRoi() {
		return mappingRtogRoi;
	}

	public void setMappingRtogRoi(Map<String, String> mappingRtogRoi) {
		this.mappingRtogRoi = mappingRtogRoi;
	}
	
	public List<ComputationResultDto> getComputationResults() {
		return computationResults;
	}

	public void setComputationResults(List<ComputationResultDto> computationResults) {
		this.computationResults = computationResults;
	}
	
	public void addComputationResult(ComputationResultDto computationResult) {
		this.computationResults.add(computationResult);
	}
	
}
