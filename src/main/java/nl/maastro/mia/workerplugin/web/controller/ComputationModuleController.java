package nl.maastro.mia.workerplugin.web.controller;

import nl.maastro.mia.workerplugin.service.Pipeline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/api/modules")
public class ComputationModuleController {
	
	@Autowired
	private Pipeline pipeline;

	@RequestMapping(method=RequestMethod.GET)
	public Set<String> getModules(){
		return pipeline.getModules();
	}

}
