package nl.maastro.mia.workerplugin.web.dto.result;

import java.util.ArrayList;
import java.util.List;

public class SparqlDataDto {
    private List<TripleDto> tripleDtos = new ArrayList<>();

    public List<TripleDto> getTripleDtos() {
          return tripleDtos;
    }

    public void setTripleDtos(List<TripleDto> tripleDtos) {
          this.tripleDtos = tripleDtos;
    }
}