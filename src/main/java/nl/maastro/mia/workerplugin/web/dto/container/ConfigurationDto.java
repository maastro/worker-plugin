package nl.maastro.mia.workerplugin.web.dto.container;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ConfigurationDto {
	
	private String name;
	private Integer outputPort;
	private String initializingServiceName;
	private Set<VolumeOfInterestDto> volumesOfInterest = new HashSet<>();	
	private List<ModuleConfigurationDto> modulesConfiguration = new ArrayList<>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(Integer outputPort) {
		this.outputPort = outputPort;
	}

	public String getInitializingServiceName() {
		return initializingServiceName;
	}

	public void setInitializingServiceName(String initializingServiceName) {
		this.initializingServiceName = initializingServiceName;
	}

	public Set<VolumeOfInterestDto> getVolumesOfInterest() {
		return volumesOfInterest;
	}

	public void setVolumesOfInterest(Set<VolumeOfInterestDto> volumesOfInterest) {
		this.volumesOfInterest = volumesOfInterest;
	}

	public List<ModuleConfigurationDto> getModulesConfiguration() {
		return modulesConfiguration;
	}

	public void setModulesConfiguration(List<ModuleConfigurationDto> modulesConfiguration) {
		this.modulesConfiguration = modulesConfiguration;
	}
	
}
