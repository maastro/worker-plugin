package nl.maastro.mia.workerplugin.web.dto.container;

public class ImageDto {
	private String location;
	private String sopUid;
	
	public ImageDto(){ } // NOSONAR
	
	public ImageDto(String location, String sopUid){
		
		this.location = location;
		this.sopUid = sopUid;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSopUid() {
		return sopUid;
	}
	public void setSopUid(String sopUid) {
		this.sopUid = sopUid;
	}
}