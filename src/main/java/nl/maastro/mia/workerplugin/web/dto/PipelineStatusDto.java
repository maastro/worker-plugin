package nl.maastro.mia.workerplugin.web.dto;

import java.util.ArrayList;
import java.util.List;

public class PipelineStatusDto {
		
	private List<ContainerStatusDto> containersInComputation = new ArrayList<>();
	
	private String lastContainerReceived;
	
	private int containersReceived;
	
	private int containersComputed;
	
	private int containersInPipelineSize;
	
	private int containersInComputationSize;
	
	private long containerTime;
	
	private long computationTime;

	private double usedMemory;
	
	private String timeFinished;

	public List<ContainerStatusDto> getContainersInComputation() {
		return containersInComputation;
	}

	public void setContainersInComputation(List<ContainerStatusDto> containersInComputation) {
		this.containersInComputation = containersInComputation;
	}

	public String getLastContainerReceived() {
		return lastContainerReceived;
	}

	public void setLastContainerReceived(String lastContainerReceived) {
		this.lastContainerReceived = lastContainerReceived;
	}

	public int getContainersReceived() {
		return containersReceived;
	}

	public void setContainersReceived(int containersReceived) {
		this.containersReceived = containersReceived;
	}

	public long getContainerTime() {
		return containerTime;
	}

	public void setContainerTime(long containerTime) {
		this.containerTime = containerTime;
	}

	public double getUsedMemory() {
		return usedMemory;
	}

	public void setUsedMemory(double usedMemory) {
		this.usedMemory = usedMemory;
	}

	public int getContainersInPipelineSize() {
		return containersInPipelineSize;
	}

	public void setContainersInPipelineSize(int containersInPipelineSize) {
		this.containersInPipelineSize = containersInPipelineSize;
	}

	public int getContainersInComputationSize() {
		return containersInComputationSize;
	}

	public void setContainersInComputationSize(int containersInComputationSize) {
		this.containersInComputationSize = containersInComputationSize;
	}

	public int getContainersComputed() {
		return containersComputed;
	}

	public void setContainersComputed(int containersComputed) {
		this.containersComputed = containersComputed;
	}

	public long getComputationTime() {
		return computationTime;
	}

	public void setComputationTime(long computationTime) {
		this.computationTime = computationTime;
	}

	public String getTimeFinished() {
		return timeFinished;
	}

	public void setTimeFinished(String timeFinished) {
		this.timeFinished = timeFinished;
	}
	
}
