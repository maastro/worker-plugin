package nl.maastro.mia.workerplugin.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.service.ImportService;
import nl.maastro.mia.workerplugin.service.Pipeline;
import nl.maastro.mia.workerplugin.service.StatusService;
import nl.maastro.mia.workerplugin.web.dto.container.ContainerDto;

@RestController
@RequestMapping("/api")
public class ComputationController {
    
	private static final Logger logger = LoggerFactory.getLogger(ComputationController.class);
	private Pipeline pipeline;
	private ImportService importService;
	private StatusService statusService;

	public ComputationController(Pipeline pipeline, ImportService importService, StatusService statusService) {
		this.pipeline = pipeline;
		this.importService = importService;
		this.statusService = statusService;
	}

	@RequestMapping(value = "/computation", method = RequestMethod.POST)
	public ResponseEntity<String> addComputation(@RequestBody ContainerDto dto){
		try {
			statusService.setProcessing(true);
			DataContainer dataContainer = importService.mapDataContainer(dto);
			if(dataContainer == null){
				statusService.setProcessing(false);
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
			else {
				pipeline.startProcessing(dataContainer);
				return new ResponseEntity<>(dataContainer.getTaskId(), HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.error("Worker cannot accept new containers", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}