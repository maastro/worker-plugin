package nl.maastro.mia.workerplugin.web.dto;

public class ContainerStatusDto {

	private String id;
	private String lastPerformedComputation;
	private String lastPerformedExport;
	private long timeInstantiation;
	private long timeComputation;
	private String error = null;
	
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastPerformedComputation() {
		return lastPerformedComputation;
	}

	public void setLastPerformedComputation(String lastPerformedComputation) {
		this.lastPerformedComputation = lastPerformedComputation;
	}

	public String getLastPerformedExport() {
		return lastPerformedExport;
	}

	public void setLastPerformedExport(String lastPerformedExport) {
		this.lastPerformedExport = lastPerformedExport;
	}

	public long getTimeInstantiation() {
		return timeInstantiation;
	}

	public void setTimeInstantiation(long timeInstantiation) {
		this.timeInstantiation = timeInstantiation;
	}

	public long getTimeComputation() {
		return timeComputation;
	}

	public void setTimeComputation(long timeComputation) {
		this.timeComputation = timeComputation;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
