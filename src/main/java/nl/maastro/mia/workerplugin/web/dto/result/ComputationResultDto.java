package nl.maastro.mia.workerplugin.web.dto.result;

import java.util.List;
import java.util.Map;

import nl.maastro.mia.workerplugin.container.VolumeOfInterest;

public class ComputationResultDto {
		String moduleName;
		String computationIdentifier;
		String error;
		Map<String,String> results;
		String version;
		List<VolumeOfInterest> volumesOfInterest;
		
		public String getComputationIdentifier() {
            return computationIdentifier;
        }

        public void setComputationIdentifier(String computationIdentifier) {
            this.computationIdentifier = computationIdentifier;
        }

        public List<VolumeOfInterest> getVolumesOfInterest() {
			return volumesOfInterest;
		}

		public void setVolumesOfInterest(List<VolumeOfInterest> volumesOfInterest) {
			this.volumesOfInterest = volumesOfInterest;
		}

		public String getError() {
			return error;
		}
		
		public void setError(String error) {
			this.error = error;
		}

		public String getVersion() {
			return version;
		}
		
		public void setVersion(String version) {
			this.version = version;
		}

		public Map<String, String> getResults() {
			return results;
		}

		public void setResults(Map<String, String> results) {
			this.results = results;
		}

		public String getModuleName() {
			return moduleName;
		}

		public void setModuleName(String moduleName) {
			this.moduleName = moduleName;
		}
}