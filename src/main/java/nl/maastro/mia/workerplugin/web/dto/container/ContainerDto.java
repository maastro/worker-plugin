package nl.maastro.mia.workerplugin.web.dto.container;

import java.util.HashMap;
import java.util.Map;

public class ContainerDto {
	
	private Map<String,String> mappingRtogRoi = new HashMap<>();
	
	private DicomPackageDto dicomPackage;
	
	private ConfigurationDto configuration;
	
	private Long id;
	
	private String calculationId;

	public ConfigurationDto getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ConfigurationDto configuration) {
		this.configuration = configuration;
	}

	public String getCalculationId() {
		return calculationId;
	}
	
	public void setCalculationId(String calculationId) {
		this.calculationId = calculationId;
	}
	
	public DicomPackageDto getDicomPackage() {
		return dicomPackage;
	}
	
	public void setDicomPackage(DicomPackageDto dicomPackage) {
		this.dicomPackage = dicomPackage;
	}
	
	public Map<String, String> getMappingRtogRoi() {
		return mappingRtogRoi;
	}
	
	public void setMappingRtogRoi(Map<String, String> mappingRtogRoi) {
		this.mappingRtogRoi = mappingRtogRoi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}