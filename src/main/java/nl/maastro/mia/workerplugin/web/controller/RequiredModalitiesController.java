package nl.maastro.mia.workerplugin.web.controller;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.workerplugin.service.RequiredModalitiesService;

@RestController
@RequestMapping("/api")
public class RequiredModalitiesController {
	
	@Autowired
	RequiredModalitiesService requiredModalitiesService;
	
	@GetMapping(value="/requiredmodalities")
	public Map<String,Set<String>> getRequiredModalities(){
		return requiredModalitiesService.getRequiredModalities();
	}

}
