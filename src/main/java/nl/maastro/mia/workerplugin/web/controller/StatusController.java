package nl.maastro.mia.workerplugin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.workerplugin.service.StatusService;

@RestController
@RequestMapping("/api")
public class StatusController {
	@Autowired
	StatusService statusService;
		
	@RequestMapping(value = "/status/available", method = RequestMethod.GET)
	public Boolean isAvailable(){
		return !statusService.isProcessing();
	}
	
	
}