package nl.maastro.mia.workerplugin.web.dto;

public class ExtConfigFileDto {
    
    private String commandParameter;
    private String fileNameOriginal;
    private String fileNameSaved;
    
    public String getCommandParameter() {
        return commandParameter;
    }
    public void setCommandParameter(String commandParameter) {
        this.commandParameter = commandParameter;
    }
    public String getFileNameOriginal() {
        return fileNameOriginal;
    }
    public void setFileNameOriginal(String fileNameOriginal) {
        this.fileNameOriginal = fileNameOriginal;
    }
    public String getFileNameSaved() {
        return fileNameSaved;
    }
    public void setFileNameSaved(String fileNameSaved) {
        this.fileNameSaved = fileNameSaved;
    }
}
