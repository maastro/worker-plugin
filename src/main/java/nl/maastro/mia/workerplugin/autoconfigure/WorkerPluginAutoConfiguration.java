package nl.maastro.mia.workerplugin.autoconfigure;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("nl.maastro.mia.workerplugin")
public class WorkerPluginAutoConfiguration {

}