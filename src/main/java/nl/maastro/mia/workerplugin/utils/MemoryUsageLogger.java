package nl.maastro.mia.workerplugin.utils;

import java.text.NumberFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryUsageLogger {
    
    private static final Logger logger = LoggerFactory.getLogger(MemoryUsageLogger.class);
    private static final int BYTES_PER_MB = 1024 * 1024;
    
    public static void logRuntimeMemoryUsage() {
        Runtime runtime = Runtime.getRuntime();
        NumberFormat numberFormat = NumberFormat.getInstance();
        
        long totalMemory = convertBytesToMB(runtime.totalMemory());
        long freeMemory = convertBytesToMB(runtime.freeMemory());
        long maxMemory = convertBytesToMB(runtime.maxMemory());
        long usedMemory = totalMemory - freeMemory;
        long totalFreeMemory = freeMemory + (maxMemory - totalMemory);
        
        logger.debug("Runtime memory usage: "
                + "max available memory (Xmx) = " + numberFormat.format(maxMemory) + " MB, "
                + "total allocated memory = " + numberFormat.format(totalMemory) + " MB, "
                + "used memory = " + numberFormat.format(usedMemory) + " MB, "
                + "free memory = " + numberFormat.format(freeMemory) + " MB, "
                + "total free memory = " + numberFormat.format(totalFreeMemory) + " MB");
    }
    
    private static long convertBytesToMB(long numberOfBytes) {
        return numberOfBytes / BYTES_PER_MB;
    }

}
