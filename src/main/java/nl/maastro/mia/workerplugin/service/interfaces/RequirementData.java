package nl.maastro.mia.workerplugin.service.interfaces;

import java.util.Set;

public class RequirementData {

	private Set<String> modalities;
	
	public Set<String> getModalities() {
		return modalities;
	}

	public void setModalities(Set<String> modalities) {
		this.modalities = modalities;
	}
}
