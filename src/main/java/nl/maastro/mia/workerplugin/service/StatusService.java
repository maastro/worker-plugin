package nl.maastro.mia.workerplugin.service;

import org.springframework.stereotype.Service;

@Service("statusservice")
public class StatusService {
	private boolean processing = false;

	public synchronized boolean isProcessing() {
		return processing;
	}

	public synchronized void setProcessing(boolean processing) throws Exception {
		if(processing && isProcessing())
			throw new Exception("Already processing");
		this.processing = processing;
	}
}