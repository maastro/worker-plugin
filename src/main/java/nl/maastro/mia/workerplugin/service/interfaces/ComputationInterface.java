package nl.maastro.mia.workerplugin.service.interfaces;

import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;

public interface ComputationInterface extends PipelineInterface {
	public ComputationResult performComputation(ComputationData computationData) throws Exception;

	public RequirementData getRequirements();
}