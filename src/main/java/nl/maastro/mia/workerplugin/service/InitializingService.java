package nl.maastro.mia.workerplugin.service;

import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import nl.maastro.mia.workerplugin.service.interfaces.PipelineInterface;

import java.util.*;

public abstract class InitializingService implements PipelineInterface {
	private static final String NOT_PRESENT = "NotPresent";

	public void initializeForAvailableVois(DataContainer dataContainer) throws Exception {
        Map<String, VolumeOfInterest> volumesOfInterest = dataContainer.getVolumesOfInterest();
        HashMap<String, VolumeOfInterest> originalVoiMap = new HashMap<>(volumesOfInterest);
        removeVoisWithNotPresent(volumesOfInterest);
		initialize(dataContainer);
		dataContainer.setVolumesOfInterest(originalVoiMap);
	}

	private Map<String,VolumeOfInterest> removeVoisWithNotPresent(Map<String, VolumeOfInterest> volumesOfInterest) {
	    volumesOfInterest.entrySet().removeIf(entry -> entry.getValue().getRois().contains(NOT_PRESENT));
		return volumesOfInterest;
	}

	protected abstract void initialize(DataContainer container) throws Exception;

	public abstract String getServiceName();

	public abstract String getVersion();
}