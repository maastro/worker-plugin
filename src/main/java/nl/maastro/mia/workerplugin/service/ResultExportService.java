package nl.maastro.mia.workerplugin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.workerplugin.web.dto.result.ComputationResultsDto;

@Service
public class ResultExportService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${microservice.resultservice:resultservice}")
	private String resultServiceName;
	
	public void sendResults(ComputationResultsDto resultsDto) throws Exception {
		String url = "http://" + resultServiceName + "/api/store";
		try {
			ResponseEntity<Boolean> response = restTemplate.postForEntity(url, resultsDto, Boolean.class);
			boolean exportSucceeded = response.getBody();
			if (!exportSucceeded) {
				throw new Exception("Failed to export results to result service");
			}
		} catch (RestClientException e) {
			throw new Exception("Unable to make REST call to result service at URL " + url, e);
		}
	}
}