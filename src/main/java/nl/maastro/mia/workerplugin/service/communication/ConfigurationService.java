package nl.maastro.mia.workerplugin.service.communication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import nl.maastro.mia.workerplugin.web.dto.ExtConfigFileDto;

@Service("configurationserviceplugin")
public class ConfigurationService {

    private final static Logger logger = LoggerFactory.getLogger(ConfigurationService.class);
    
    @Value("${microservice.configurationservice:configservice}")
    private String configserviceName;

    private static final String HTTP = "http://";

    @Autowired
    private RestTemplate restTemplate;

    public Map<String, Path> downloadExtConfigFiles(List<ExtConfigFileDto> configFiles, Path tempFolderPath) throws IOException {
        HashMap<String, Path> extconfigFilePaths = new HashMap<>();
        for (ExtConfigFileDto configFile : configFiles) {
            String commandParameter = configFile.getCommandParameter();
            String configFileName = configFile.getFileNameSaved();
            Path configFilePath = tempFolderPath.resolve(configFileName);
            if (!configFilePath.toFile().exists()) {
                configFilePath = downloadFile(configFileName, tempFolderPath);
            };
            extconfigFilePaths.put(commandParameter, configFilePath.toAbsolutePath().normalize());
        }
        return extconfigFilePaths;
    }


    private Path downloadFile(String fileName, Path tempFolderPath) throws IOException {
        logger.debug("Downloading extconfig file: " + fileName);
        String url = HTTP + configserviceName + "/api/extconfig/file";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("fileName", fileName);

        restTemplate.getMessageConverters().add(
                new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<byte[]> response = restTemplate.exchange(
                builder.build().encode().toUri(), HttpMethod.GET, entity, byte[].class);

        if (response.getStatusCode() == HttpStatus.OK) {
            Path filePath = tempFolderPath.resolve(fileName); 
            Files.write(filePath, response.getBody());
            return filePath;
        }
        else {
            throw new IOException("Unable to download file '" + fileName + "' at URL " + url);
        }
    }
}
