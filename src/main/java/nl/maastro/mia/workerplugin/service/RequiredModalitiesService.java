package nl.maastro.mia.workerplugin.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;

@Service
public class RequiredModalitiesService {
	
	private static final Logger logger = LoggerFactory.getLogger(RequiredModalitiesService.class);
	
	@Autowired 
	private Pipeline pipeline;

	/**
	 * Get required modalities for this worker, based on all computations it can perform.
	 * @return Map containing the required modalities as values, with the keys being names 
	 * for each module. 
	 */
	public Map<String, Set<String>> getRequiredModalities(){
		Map<String,Set<String>> requiredModalities = new HashMap<>();
		for(Entry<String, ComputationInterface> entry : pipeline.getModulesServices().entrySet()){
			RequirementData requirementData = entry.getValue().getRequirements();
			if(requirementData==null){
				logger.warn("requirementData not implemented for service:" +entry.getKey());
				requiredModalities.put(entry.getKey(), new HashSet<>());
			}
			else{
				Set<String> requiredModalitiesModule = requirementData.getModalities();
				requiredModalities.put(entry.getKey(), requiredModalitiesModule);
			}
		}
		return requiredModalities;
	}

}
