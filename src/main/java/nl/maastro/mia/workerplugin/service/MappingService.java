package nl.maastro.mia.workerplugin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.container.VolumeOfInterest;

@Service("mappingservice")
public class MappingService {
	
	public Map<String, VolumeOfInterest> mapRtogsToRois(
			Map<String, VolumeOfInterest> rtogVolumesOfInterest, 
			Map<String, String> rtogMapping) {	

		Map<String, VolumeOfInterest> roiVolumesOfInterest = new HashMap<>();
		
		for (String voiName : rtogVolumesOfInterest.keySet()) {
			
			VolumeOfInterest rtogVolumeOfInterest = rtogVolumesOfInterest.get(voiName);
			
			List<String> rois = new ArrayList<>();
			List<String> rtogs = rtogVolumeOfInterest.getRois();
			rtogs.forEach(rtog -> rois.add(rtogMapping.get(rtog)));

			VolumeOfInterest roiVolumeOfInterest = new VolumeOfInterest();
			roiVolumeOfInterest.setName(rtogVolumeOfInterest.getName());
			roiVolumeOfInterest.setRois(rois);
			roiVolumeOfInterest.setOperators(rtogVolumeOfInterest.getOperators());
			
			roiVolumesOfInterest.put(voiName, roiVolumeOfInterest);
		}
		
		return roiVolumesOfInterest;
	}
}
