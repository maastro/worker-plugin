package nl.maastro.mia.workerplugin.service.communication;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import nl.maastro.mia.workerplugin.container.Image;

@Service("fileserviceplugin")
public class FileService {

    private final static Logger logger = LoggerFactory.getLogger(FileService.class);

    @Value("${microservice.fileservice:fileservice}")
    private String fileServiceName;

    private static final String HTTP = "http";
    private static final String DCM_EXTENSION = ".dcm";
    private static final String API_IMAGE_DOWNLOAD = "/api/image/{sopInstanceUid}/download";

    @Autowired
    private RestTemplate restTemplate;

    public void downloadModality(String modality, Map<String, List<Image>> images, Path path)
            throws NoSuchElementException, IOException {

        List<Image> modalityImages = images.get(modality);
        if (modalityImages==null || modalityImages.isEmpty()) {
            throw new NoSuchElementException("No images found for modality: " + modality);
        }

        for (Image image : modalityImages){
            downloadFile(path, image);
        }
    }

    public void downloadFile(Path path, Image image) throws IOException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Path newLocation = downloadFile(path, image.getSopUid());
        image.setLocation(newLocation.toString());
        stopWatch.stop();
        logger.debug("Downloaded file "+ image.getSopUid()+" in " + stopWatch.getTotalTimeMillis() + "ms");
    }

    private Path downloadFile(Path path, String sopUid) throws IOException {
        Path newLocation = path.toAbsolutePath().normalize().resolve(sopUid + DCM_EXTENSION);
        if(newLocation.toFile().isFile())
            return newLocation;

        logger.debug("Downloading DICOM file for sopUid: " + sopUid);

        URI uri = UriComponentsBuilder.newInstance()
                .scheme(HTTP)
                .host(fileServiceName)
                .path(API_IMAGE_DOWNLOAD)
                .buildAndExpand(sopUid)
                .toUri();
        
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<byte[]> response = restTemplate.exchange(uri, HttpMethod.GET, entity, byte[].class);

        if (response.getStatusCode() == HttpStatus.OK) {
            Files.write(newLocation, response.getBody());
            return newLocation;
        }else {
            throw new IOException("Unable to download file at URL: " + uri.toString());
        }
    }
}
