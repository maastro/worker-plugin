package nl.maastro.mia.workerplugin.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import nl.maastro.dicomUtils.communication.MaastroStoreScu;

@Service
public class FileExportService {
	
	@Autowired
	private EurekaClient eurekaClient;
	
	private MaastroStoreScu storeScu;
	
	@Value("${microservice.fileservice:fileservice}")
	private String fileServiceName;
	
	@Value("${internalaetitle:FILESERVICE}")
	private String internalAeTitle;
	
	@Value("${microservice.fileservice.aetitle:FILESERVICE}")
	private String remoteAeTitle;
	
	public void sendFiles(List<File> files, Integer remotePort) throws Exception {
		try {
			setupStoreScu(remotePort);
			boolean storeSucceeded = storeScu.storeFiles(files);
			if (!storeSucceeded) {
				throw new Exception("Failed to store dicom files at file service via storescu.");
			}
		} catch (IOException e) {
			throw new Exception("Failed to store dicom files at file service via storescu.", e);
		}
	}
	
	public void setupStoreScu(Integer remotePort) throws Exception {
		if (storeScu == null) {
			try {
				String remoteHost = getFileServiceHost();
				storeScu = new MaastroStoreScu(internalAeTitle, remoteAeTitle, remoteHost, remotePort);
			} catch (Exception e) {
				throw new Exception("Unable to create a storeScu.", e);
			}
		} else {
			storeScu.setRemotePort(remotePort);
		}
	}
	
	public String getFileServiceHost() throws Exception {
		String fileServiceHost = null;
		try {
			Application fileServiceApplication = eurekaClient.getApplication(fileServiceName);
			List<InstanceInfo> fileServiceInstances = fileServiceApplication.getInstances();
			fileServiceHost = fileServiceInstances.get(0).getHostName();
		} catch (Exception e) {
			throw new Exception("Unable to retrieve file service host address", e);
		}
		return fileServiceHost;
	}
	
}
