package nl.maastro.mia.workerplugin.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.container.Computation;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;

@Service
public class ComputationService {
    
	private static final Logger logger = LoggerFactory.getLogger(ComputationService.class);
	private static final String NOT_PRESENT = "NotPresent";

	/**
	 * Check for computation data whether images, Volume of Interest and structures are valid.
	 * @param data An object containing data to compute.
	 * @return Result object containing the appropriate error if necessary. 
	 */
	public ComputationResult checkDataAvailability(ComputationData data) {
		ComputationResult result = new ComputationResult();
		if(data.getImages() == null || data.getImages().isEmpty()){
			String errorMessage = "No images in data.";
			result.setError(errorMessage);
			logger.error(errorMessage);
		}
		
		if(!structuresPresent(data.getVolumesOfInterest())){
			String errorMessage = "Cannot preform computation, missing rtogs for patientId : "+ 
					data.getPatientId();
			result.setError(errorMessage);
			logger.error(errorMessage);
		}

		return result;
	}

	private boolean structuresPresent(List<VolumeOfInterest> volumeOfInterestList){
		for (VolumeOfInterest volumeOfInterest : volumeOfInterestList) {
			for (String roi : volumeOfInterest.getRois()){
				if (roi == null){
					logger.info("Missing RTOG for " + roi + ", calculation skipped.");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Perform a computation, using the supplied computation module on the computation data.
	 * @param currentService Module to use for the computation.
	 * @param computation Computation to perform.
	 * @param computationData Data to compute on.
	 * @return object containing all results of the computation.
	 */
	public ComputationResult compute(ComputationInterface currentService,
			Computation computation,
			ComputationData computationData) throws Exception {
		ComputationResult result = new ComputationResult();

		for (VolumeOfInterest volumeOfInterest : computationData.getVolumesOfInterest()) {
			if (volumeOfInterest.getRois().contains(NOT_PRESENT)) {
				logger.warn("Volume of interest " 
						+ volumeOfInterest
						+ " is incomplete for computation identifier "
						+ computation);
				result.addComputationData(computationData,
						currentService.getServiceName(),
						computation);
				return result;
			}
		}
		
		result = checkDataAvailability(computationData);
		if(result.getError() != null){
			result.addComputationData(computationData,
					currentService.getServiceName(),
					computation);
			return result;
		}

		result = currentService.performComputation(computationData);
		result.addComputationData(computationData,
				currentService.getServiceName(),
				computation);
		return result;
	}
}