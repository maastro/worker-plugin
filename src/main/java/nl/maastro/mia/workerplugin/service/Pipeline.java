package nl.maastro.mia.workerplugin.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.container.Computation;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.container.ModuleConfiguration;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import nl.maastro.mia.workerplugin.enumeration.ContainerStatus;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.utils.MemoryUsageLogger;

@Service
public class Pipeline{
    
	private static final Logger logger = LoggerFactory.getLogger(Pipeline.class);
	
	private ApplicationContext context;
	private ComputationService computationService;
	private SchedulingService schedulingService;
	private ExportService exportService;
	private StatusService statusService;
	private MappingService mappingService;
	private MatlabService matlabService;

	public Pipeline(ApplicationContext context, ComputationService computationService, SchedulingService schedulingService, ExportService exportService, StatusService statusService, MappingService mappingService, MatlabService matlabService) {
		this.context = context;
		this.computationService = computationService;
		this.schedulingService = schedulingService;
		this.exportService = exportService;
		this.statusService = statusService;
		this.mappingService = mappingService;
		this.matlabService = matlabService;
	}

	@Value("${deleteTempFolder:true}")
	private boolean deleteTempFolder;
	
	@Value("${tempPath:.\\temp}")
	public String tempPath;

	class Bekele implements Runnable {
		DataContainer dataContainer;

		public Bekele(DataContainer dataContainer){
			this.dataContainer = dataContainer;
		}

		@Override
		public void run() {
			processContainer(dataContainer);
		}
	}

	public void startProcessing(DataContainer dataContainer) {

		Bekele kenenisa = new Bekele(dataContainer);
		logger.debug("Set....\r\n" + 
				"   _____.\r\n" + 
				" ~(_]---'\r\n" + 
				"/_(U");
		new Thread(kenenisa).start();
	}
	
	private void processContainer(DataContainer dataContainer) {
		logger.info("Processing container " + dataContainer.getContainerId());
		MemoryUsageLogger.logRuntimeMemoryUsage();
		String tempFolderPath = tempPath + "\\" + dataContainer.getContainerId().toString();
		try {
			createTempFolder(tempFolderPath);
			dataContainer.setTempPath(tempFolderPath);
			dataContainer = applyRtogToRoiMappings(dataContainer);
			dataContainer = initialize(dataContainer);
			List<ComputationResult> computationResults = compute(dataContainer);
			exportService.export(dataContainer, computationResults);
			logger.info("Container " + dataContainer.getContainerId() + " has been processed successfully.");
		} catch (Exception e) {
			logger.error("Uncaught error while processing getContainerId=" + dataContainer.getContainerId(), e);
			schedulingService.updateTaskStatus(dataContainer.getTaskId(),
					ContainerStatus.CALCULATIONERROR);
		} catch (OutOfMemoryError e){
			logger.error("OUT OF MEMORY ERROR! while processing getContainerId=" + dataContainer.getContainerId(), e);
			schedulingService.updateTaskStatus(dataContainer.getTaskId(),
					ContainerStatus.CALCULATIONERROR);
		} catch (Error e){
			logger.error("Error! while processing getContainerId=" + dataContainer.getContainerId(), e);
			schedulingService.updateTaskStatus(dataContainer.getTaskId(),
					ContainerStatus.CALCULATIONERROR);
		}
		finally {
			if(deleteTempFolder)
				deleteTempFolder(tempFolderPath);
		    MemoryUsageLogger.logRuntimeMemoryUsage();
			matlabService.dispose(dataContainer);
			MemoryUsageLogger.logRuntimeMemoryUsage();
			try {
				statusService.setProcessing(false);
			} catch (Exception e) {
				logger.error("Error changing processing state", e);
			}
		}
	}
	
	private void createTempFolder(String tempFolderPath) {
		try {
			new File(tempFolderPath).mkdirs();
		} catch (Exception e) {
			logger.error("Unable to create temporary folder '" + tempFolderPath + "'", e);
		}
	}
	
	private void deleteTempFolder(String tempFolderPath) {
		try {
			File tempFolder = new File(tempFolderPath);
			if (tempFolder.exists()) {
				FileUtils.deleteDirectory(tempFolder);
			}
		} catch (Exception e) {
			logger.error("Unable to delete temporary folder '" + tempFolderPath + "'", e);
		}
	}

	private DataContainer applyRtogToRoiMappings(DataContainer dataContainer) {
		Map<String, String> rtogMappings = dataContainer.getMappingRtogRois();
		Map<String, VolumeOfInterest> rtogVolumesOfInterest = dataContainer.getVolumesOfInterest();
		Map<String, VolumeOfInterest> roiVolumesOfInterest = mappingService.mapRtogsToRois(rtogVolumesOfInterest, rtogMappings);
		dataContainer.setVolumesOfInterest(roiVolumesOfInterest);
		return dataContainer;
	}

	private DataContainer initialize(DataContainer dataContainer) throws Exception {
		InitializingService initializingService = null;
		if(dataContainer.getInitializingServiceName() != null){
			initializingService = context
					.getBeansOfType(InitializingService.class)
					.get(dataContainer.getInitializingServiceName());
		}

		if(initializingService==null){
			Map<String,InitializingService> initializingServices =
					context
					.getBeansOfType(InitializingService.class);
			if(!initializingServices.values().isEmpty()){
				initializingService = initializingServices.values().iterator().next();
			}
		}
		if(initializingService==null){
			logger.warn("Could not find initializing service "
					+ dataContainer.getInitializingServiceName()
					+ " in current spring application context.");
			throw new Exception("Error initializing datacontainer");
		}

		initializingService.initializeForAvailableVois(dataContainer);
		return dataContainer;
	}
	
	private List<ComputationResult> compute(DataContainer dataContainer) throws Exception {
		List<ComputationResult> results = new ArrayList<>();
		for(ModuleConfiguration moduleConfiguration :
			dataContainer.getModuleConfigurations()){
			List<ComputationResult> moduleResults = 
					runModule(moduleConfiguration, dataContainer);
			results.addAll(moduleResults);
		}
		return results;
	}

	private List<ComputationResult> runModule(ModuleConfiguration moduleConfiguration, 
			DataContainer dataContainer) throws Exception {
		List<ComputationResult> results = new ArrayList<>();
		String moduleName = moduleConfiguration.getModuleName();
		logger.info("Performing calculations for moduleName: "+ moduleName);

		ComputationInterface currentService = context
				.getBeansOfType(ComputationInterface.class)
				.get(moduleName);

		if(currentService == null){
			logger.error("Unable to get bean from applicationContext of type ComputationService and name: "
					+ moduleName );
			ComputationResult result = new ComputationResult();
			result.setError("Unable to get bean from applicationContext of type ComputationService and name: "
					+ moduleName );
			result.setModuleName(moduleName);
			results.add(result);
			return results;
		}

		for(Computation computation : moduleConfiguration.getComputations()){
			logger.info("Performing calculations for computation: "+ computation.getIdentifier());
			MemoryUsageLogger.logRuntimeMemoryUsage();
			
			ComputationData computationData = new ComputationData(dataContainer, computation);
			ComputationResult result = computationService.compute(currentService,
					computation,
					computationData);
			
			logger.info("Computation " + computation.getIdentifier() + " finished");
			MemoryUsageLogger.logRuntimeMemoryUsage();

			if (result.getError() != null) {
				schedulingService.updateTaskStatus(dataContainer.getTaskId(),
						ContainerStatus.CALCULATIONERROR);
			}

			results.add(result);
			dataContainer.getData().putAll(result.getGeneratedObjects());
			dataContainer.getDisposables().addAll(result.getDisposables());
		}
		return results;
	}

	public Set<String> getModules(){
		return context.getBeansOfType(ComputationInterface.class).keySet();
	}

	public Map<String, ComputationInterface> getModulesServices(){
		return (Map<String, ComputationInterface>) context.getBeansOfType(ComputationInterface.class);
	}
}