package nl.maastro.mia.workerplugin.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.container.Image;
import nl.maastro.mia.workerplugin.enumeration.ContainerStatus;
import nl.maastro.mia.workerplugin.web.dto.result.ComputationResultDto;
import nl.maastro.mia.workerplugin.web.dto.result.ComputationResultsDto;

@Service("exportservice")
public class ExportService {
    
	private static final Logger logger = LoggerFactory.getLogger(ExportService.class);

	@Autowired
	private SchedulingService schedulingService;

	@Autowired
	private FileExportService fileService;

	@Autowired
	private ResultExportService resultService;

	/**
	 * Export results and DICOM files for each computation to result service
	 * and file service, respectively.
	 * @param dataContainer Container computations have been performed on.
	 * @param computationResults List of the ComputationResult objects.
	 */
	public void export(DataContainer dataContainer, List<ComputationResult> computationResults) {
		
		boolean resultExportSucceeded = true;
		try {
		    List<ComputationResult> resultsToExport = computationResults.stream()
		            .filter(result -> result.isExportResult())
		            .collect(Collectors.toList());
		    if (!resultsToExport.isEmpty()) {
		        exportResultsToResultService(dataContainer, resultsToExport);
		    }
		} catch (Exception e) {
			resultExportSucceeded = false;
			logger.error("Error exporting results to result service", e);
		}
		
		boolean fileExportSucceeded = true;
		try {
		    List<File> dicomFiles = new ArrayList<>();
			for (ComputationResult result : computationResults) {
			    if (result.isExportDicom()) {
    				List<File> generatedDicomFiles = result.getGeneratedDicomFiles().stream().map(filepath -> new File(filepath)).collect(Collectors.toList());
    				dicomFiles.addAll(generatedDicomFiles);
			    }
			}
			if (!dicomFiles.isEmpty()) {
			    exportFilesToFileService(dicomFiles, dataContainer.getOutputPort());
			}
		} catch (Exception e) {
			fileExportSucceeded = false;
			logger.error("Error exporting DICOM files to file service", e);
		}
		
		if (resultExportSucceeded && fileExportSucceeded) {
			schedulingService.updateTaskStatus(dataContainer.getTaskId(), ContainerStatus.DONE);
		} else {
			schedulingService.updateTaskStatus(dataContainer.getTaskId(), ContainerStatus.EXPORTERROR);
		}		
	}

	private void exportResultsToResultService(DataContainer dataContainer, 
			List<ComputationResult> computationResults) throws Exception {
		logger.info("Exporting results to result service.");
		ComputationResultsDto resultsDto = getComputationResultsDto(dataContainer, computationResults);
		resultService.sendResults(resultsDto);
		logger.info("Successfully exported results to result service.");
	}

	public void exportFilesToFileService(List<File> dicomFiles, 
			Integer outputPort) throws Exception {

		logger.info("Exporting DICOM files to fileservice.");
		fileService.sendFiles(dicomFiles, outputPort);
		logger.info("Successfully exported DICOM files to file service.");
	}

	public List<File> createFileList(List<String> filePaths) {
		List<File> fileList = new ArrayList<>();
		for (String pathName : filePaths) {
			try {
				File f = new File(pathName); 
				fileList.add(f);
			} catch (Exception e) {
				logger.error("Unable to create a File object for path: " + pathName, e);
			}
		}
		return fileList;
	}

	private ComputationResultsDto getComputationResultsDto(DataContainer dataContainer, 
			List<ComputationResult> computationResults) {
        ComputationResultsDto resultsDto = new ComputationResultsDto();
        resultsDto.setTaskId(dataContainer.getTaskId());
        resultsDto.setContainerId(dataContainer.getContainerId().toString());
        resultsDto.setPatientId(dataContainer.getPatientId());
        resultsDto.setPlanLabel(dataContainer.getPlanLabel());
        resultsDto.setSopUids(getImagesDto(dataContainer.getImages()));
        resultsDto.setMappingRtogRoi(dataContainer.getMappingRtogRois());
        for (ComputationResult result : computationResults) {
            resultsDto.addComputationResult(getComputationResultDto(result));
        }
        return resultsDto;
	}

	private Map<String, List<String>> getImagesDto(Map<String, List<Image>> images){
		Map<String, List<String>> imagesDto = new HashMap<>();
		for(Entry<String, List<Image>> entry : images.entrySet()){
			List<String> sopUids = new ArrayList<>();
			for(Image image : entry.getValue()){
				sopUids.add(image.getSopUid());
			}
			imagesDto.put(entry.getKey(), sopUids);
		}
		return imagesDto;
	}

	private ComputationResultDto getComputationResultDto(ComputationResult result) {
		ComputationResultDto resultDto = new ComputationResultDto();
		resultDto.setComputationIdentifier(result.getComputationIdentifier());
		resultDto.setVolumesOfInterest(result.getVolumesOfInterest());
		resultDto.setResults(result.getValues());
		resultDto.setError(result.getError());
		resultDto.setVersion(result.getVersion());
		resultDto.setModuleName(result.getModuleName());
		return resultDto;
	}
}
