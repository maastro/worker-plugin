package nl.maastro.mia.workerplugin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import nl.maastro.mia.workerplugin.container.Computation;
import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.container.Image;
import nl.maastro.mia.workerplugin.container.ModuleConfiguration;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import nl.maastro.mia.workerplugin.service.communication.FileService;
import nl.maastro.mia.workerplugin.web.dto.container.ComputationDto;
import nl.maastro.mia.workerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.workerplugin.web.dto.container.ImageDto;
import nl.maastro.mia.workerplugin.web.dto.container.ModuleConfigurationDto;
import nl.maastro.mia.workerplugin.web.dto.container.VolumeOfInterestDto;

@Service("importservice")
public class ImportService {

	private static final Logger logger = LoggerFactory.getLogger(ImportService.class);

	@Autowired
	@Qualifier("fileserviceplugin")
	private FileService fileService;

	/**
	 * create a dataContainer based on the incoming dto object.
	 * @param dto container data object to map.
	 * @return datacontainer based on the dataobject.
	 */
	public DataContainer mapDataContainer(ContainerDto dto){
		if(dto.getDicomPackage() == null){
			return null;
		}
		DataContainer dataContainer = createDataContainer(dto);
		logger.info("New Container: " + dataContainer);
		return dataContainer;
	}

	private DataContainer createDataContainer(ContainerDto dto) {
		DataContainer dataContainer = new DataContainer();
		if(dto.getConfiguration()==null){
			logger.error("No configuration set for dto containerId: "+dto.getId());
			return null;
		}
		dataContainer.setVolumesOfInterest(mapVoi(dto.getConfiguration().getVolumesOfInterest()));
		dataContainer.setModuleConfigurations(mapConfiguration(dto.getConfiguration().getModulesConfiguration()));
		dataContainer.setImages(mapImages(dto.getDicomPackage().getImages()));
		dataContainer.setMappingRtogRois(dto.getMappingRtogRoi());
		dataContainer.setPatientId(dto.getDicomPackage().getPatientId());
		dataContainer.setPlanLabel(dto.getDicomPackage().getPlanlabel());
		dataContainer.setRoiNames(dto.getDicomPackage().getRoiNames());
		dataContainer.setContainerId(dto.getId());
		dataContainer.setInitializingServiceName(dto.getConfiguration().getInitializingServiceName());
		dataContainer.setOutputPort(dto.getConfiguration().getOutputPort());
		
		return dataContainer;
	}
	
	private List<ModuleConfiguration> mapConfiguration(List<ModuleConfigurationDto> dtoList){
		List<ModuleConfiguration> moduleList = new ArrayList<>();
		for(ModuleConfigurationDto dto : dtoList){
			List<Computation> newComputations = mapComputations(dto.getComputations());
			String moduleName = dto.getModuleName();
			ModuleConfiguration moduleConfig = new ModuleConfiguration();
			moduleConfig.setModuleName(moduleName);
			moduleConfig.setComputations(newComputations);
			moduleList.add(moduleConfig);
		}
		return moduleList;
	}
	
	private List<Computation> mapComputations(List<ComputationDto> dtoList){
		List<Computation> computationList = new ArrayList<>();
		for(ComputationDto dto : dtoList){
			Computation computation = new Computation();
			computation.setConfiguration(dto.getConfiguration());
			computation.setVolumeOfInterestNames(dto.getVolumeOfInterestNames());
			computation.setIdentifier(dto.getIdentifier());
			computation.setInputModalities(dto.getInputModalities());
			computation.setOutputModalities(dto.getOutputModalities());
			computationList.add(computation);
		}
		return computationList;
	}
	
	private Map<String, List<Image>> mapImages(Map<String, List<ImageDto>> imageDtoList){
		Map<String, List<Image>> images = new HashMap<>();
		for(Entry<String, List<ImageDto>> entry : imageDtoList.entrySet()){

			List<Image> imageList = new ArrayList<>();
			for(ImageDto dto : entry.getValue()){
				Image image = new Image();
				image.setLocation(dto.getLocation());
				image.setSopUid(dto.getSopUid());
				imageList.add(image);
			}
			images.put(entry.getKey(), imageList);
		}
		return images;
	}
	
	private Map<String, VolumeOfInterest> mapVoi(Set<VolumeOfInterestDto> dtoSet){
		Map<String, VolumeOfInterest> voiMap = new HashMap<>();
		if (dtoSet == null) {
            return voiMap; 
        }
		for(VolumeOfInterestDto dto : dtoSet){
			VolumeOfInterest voi = new VolumeOfInterest();
			voi.setName(dto.getName());
			voi.setOperators(dto.getOperators());
			voi.setRois(dto.getRtogs());
			voiMap.put(dto.getName(), voi);
		}
		return voiMap;
	}

}