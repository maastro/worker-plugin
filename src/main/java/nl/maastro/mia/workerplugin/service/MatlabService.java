package nl.maastro.mia.workerplugin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mathworks.toolbox.javabuilder.Disposable;

import nl.maastro.mia.workerplugin.container.DataContainer;

@Service
public class MatlabService {
    private static final Logger logger = LoggerFactory.getLogger(MatlabService.class);

    /**
     * Dispose the matlab data in a datacontainer. Unfortunately, this data is not
     * automatically garbage collected.
     * @param dataContainer dataContainer that has finished it's computations and export.
     */
    public void dispose(DataContainer dataContainer) {
        Disposable matlabInstance = dataContainer.getMatlabInstance();
        for(Disposable disposable : dataContainer.getDisposables()) {
            if(disposable != null){
                logger.debug("Disposing MATLAB disposable.");
                disposable.dispose();
            }
        }
        if(matlabInstance != null){
            logger.info("Disposing MATLAB instance.");
            matlabInstance.dispose();
        }
    }
}
