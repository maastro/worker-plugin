package nl.maastro.mia.workerplugin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.workerplugin.enumeration.ContainerStatus;

@Service
public class SchedulingService {
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulingService.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${microservice.schedulingservice:scheduler}")
	private String schedulingServiceName;
	
	public void updateTaskStatus(String taskId, ContainerStatus newStatus) {
		
		logger.info("Updating task status at scheduling service to " + newStatus);
		
		String url = "http://" + schedulingServiceName + "/api/computation/" 
                + taskId + "?taskStatus=" + newStatus;
		
		try {
			boolean updateSucceeded = restTemplate.getForObject(url, Boolean.class);
			if (!updateSucceeded) {
				logger.error("Failed to update task status at scheduling service");
			}
		} catch (RestClientException e) {
			logger.error("Unable to make REST call to scheduling service", e);
		}
	}
}
