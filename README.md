# Worker Plugin #

The Worker Plugin is a Spring Boot Plugin for creating MIA Workers. It enables developers to build their own Java and/or MATLAB based MIA Workers to automate and distribute batch calculations within the [MIA framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home).

## Prerequisites ##

- Java 8
- 4 GB RAM
- Modern browser 
- Matlab MCR 9 (see documentation)

## Creating a custom Worker ##

- Setup a [Spring Boot](https://start.spring.io/) project and add the following dependency to your pom:

```xml
	<dependency>
    	<groupId>nl.maastro.mia.workerplugin</groupId>
    	<artifactId>workerplugin</artifactId>
    	<version>VERSION</version>
	</dependency>
```

- To build a maven project with the workerplugin, you need a non-maven central dependency javabuilder.jar:

```xml
	<dependency>
		<groupId>com.mathworks.toolbox</groupId>
		<artifactId>javabuilder</artifactId>
		<version>2015.2.0</version>
	</dependency>
```

- To add the javabuilder to your local maven repository

	1. Download and install [Matlab Runtime v9.0](http://nl.mathworks.com/products/compiler/mcr/)
	2. Deploy javabuilder.jar to your maven repository `C:\Program Files\MATLAB\MATLAB Runtime\v90\toolbox\javabuilder\jar\javabuilder.jar`
    
```
    mvn install:install-file -Dfile=C:\Program Files\MATLAB\MATLAB Runtime\v90\toolbox\javabuilder\jar\javabuilder.jar -DgroupId=com.mathworks.toolbox -DartifactId=javabuilder -Dversion=2015.2.0 -Dpackaging=jar
```

The main application.class and the testsuite require some additional annotations and properties to work.

- The worker plugin is autoconfigured and will start services and controllers to run your calculations. Define the application name, this name is used to schedule calculations.

Configuration in "src/main/resources/application.yml"

	server.port: 8081
	spring:
	  application:
	    name: workername

- The initializing interface can be implemented to set the Matlab instance and data:

```java
	@Service("createStructureData")
	public class CreateStructureDataService implements InitializingInterface {
```	

- Implement the ComputationInterface to perform computations on your datacontainer.

```java
	@Service("DvhDoseComputation")
	public class DvhDoseComputationService implements ComputationInterface {
    
	private static final Logger LOGGER = Logger.getLogger(DvhDoseComputationService.class);
	private static final String SERVICE_NAME = "DvhDoseComputation";
   
	@Autowired
	private GenericComputationToolService genericComputationToolService;
    
	@Override
	public ComputationResult performComputation(Computation computation, DataContainer dataContainer) {
        ComputationResult result = new ComputationResult();
```
 
 The service name is used to set the corresponding configuration.
 
- The required modalities can be validated in the manager:

```java
	@Override
	public RequirementData getRequirements() {
	    RequirementData reqData = new RequirementData();
	    reqData.setModalities(new HashSet<>(Arrays.asList(new String[]{"CT", "RTSTRUCT", "RTDOSE"})));
	    return reqData;
	}
```

- As a convention we log the version in the log file

```java
	@SpringBootApplication
	@EnableEurekaClient
	public class DgrtWorkerApplication {
		private static final Logger LOGGER = Logger.getLogger(DgrtWorkerApplication.class);
		public static final String name = DgrtWorkerApplication.class.getPackage().getName();
		public static final String version = DgrtWorkerApplication.class.getPackage().getImplementationVersion();

		private static final String nameGeneric = Pipeline.class.getPackage().getName();
		private static final String versionGeneric = Pipeline.class.getPackage().getImplementationVersion();
    	
		public static void main(String[] args) {
			SpringApplication.run(DgrtWorkerApplication.class, args);
			LOGGER.info("**** DGRT worker VERSION **** : " + name + " " +  version );
			LOGGER.info("**** Pipeline VERSION **** : " + nameGeneric + " " +  versionGeneric );
		}
	}
```